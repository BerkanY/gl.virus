﻿namespace GL.Virus.JustFillIt.Remover
{
    using System;
    using System.IO;

    internal class Program
    {
        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        internal static void Main()
        {
            DirectoryInfo Directory = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.Desktop));

            foreach (FileInfo File in Directory.GetFiles("Bullshit-*.bin"))
            {
                File.Delete();
            }
        }
    }
}