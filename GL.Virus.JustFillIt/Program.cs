﻿namespace GL.Virus.JustFillIt
{
    using System;
    using System.IO;
    using System.Text;

    internal class Program
    {
        internal static int CountOfFiles = 10;

        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        internal static void Main()
        {
            Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.Desktop));

            for (int i = 1; i <= Program.CountOfFiles; i++)
            {
                using (FileStream Stream = File.Create(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Bullshit-" + i + ".bin"))
                {
                    Stream.Write(Encoding.UTF8.GetBytes("Hello from GobelinLand !"), 0, 24);
                }
            }
        }
    }
}